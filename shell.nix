with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "env";
  buildInputs = [
    pkgconfig alsaLib
  ];

  shellHook = ''
    export RUSTFLAGS="-C target-cpu=native"
  '';
}

