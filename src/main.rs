use std::io::Cursor;
use rodio::{Decoder, Sink, Source};

fn play(file: &'static [u8], volume: f32) -> impl rodio::Source<Item = i16> {
    Decoder::new(Cursor::new(file))
        .unwrap()
        .amplify(volume)
        .repeat_infinite()
}

fn main() {
    let device = rodio::default_output_device().unwrap();
    let source = play(include_bytes!("../resources/birds.wav"), 1.)
        .mix(play(include_bytes!("../resources/beach.wav"), 0.3))
        .mix(play(include_bytes!("../resources/forest_wind.wav"), 1.));
    let sink = Sink::new(&device);
    sink.append(source);
    sink.sleep_until_end();
}
